-- Answers to Exercise 2 here
DROP TABLE IF EXISTS tb_ex05_JSON;

CREATE TABLE IF NOT EXISTS tb_ex05_JSON (
  username   VARCHAR(64),
  first_name VARCHAR(64),
  last_name  VARCHAR(64),
  email      VARCHAR(64),
  PRIMARY KEY (username)
);

INSERT INTO tb_ex05_JSON (username, first_name, last_name, email) VALUE
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('programmer2', 'Peter', NULL , NULL ),
  ('programmer3', 'Pete', NULL ,NULL ),
  ('programmer4', NULL ,'Peterson', NULL );
INSERT INTO tb_ex05_JSON VALUE ('programmer1', 'Peter', NULL , NULL);
SELECT * FROM tb_ex05_JSON;