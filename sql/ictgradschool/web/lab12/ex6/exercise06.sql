-- Answers to Exercise 6 here
# -- Answers to Exercise 3 here

DROP TABLE IF EXISTS customs_dbtable2;

CREATE TABLE IF NOT EXISTS customs_dbtable2(
  name VARCHAR(64),
  gender VARCHAR(6),
  year_born INT,
  year_joined INT,
  number_of_hires INT,
  PRIMARY KEY (name)
);

INSERT INTO customs_dbtable2 (name, gender, year_born, year_joined, number_of_hires) VALUE
  ( 'Peter Jackson', 'male', 1961, 1997, 17000 ),

  ( 'Jane Campion', 'female', 1954, 1980, 30000 ),

  ( 'Roger Donaldson', 'male', 1945, 1980, 12000 ),

  ( 'Temuera Morrison', 'male', 1960, 1995, 15500 ),

  ( 'Russell Crowe', 'male', 1964, 1990, 10000 ),

  ( 'Lucy Lawless', 'female', 1968, 1995, 5000 ),

  ( 'Michael Hurst', 'male', 1957, 2000, 15000 ),

  ( 'Andrew Niccol', 'male', 1964, 1997, 3500 ),

  ( 'Kiri Te Kanawa', 'female', 1944, 1997, 500 ),

  ( 'Lorde', 'female', 1996, 2010, 1000 ),

  ( 'Scribe', 'male', 1979, 2000, 5000 ),

  ( 'Kimbra', 'female', 1990, 2005, 7000 ),

  ( 'Neil Finn', 'male', 1958, 1985, 6000 ),

  ( 'Anika Moa', 'female', 1980, 2000, 700 ),

  ( 'Bic Runga', 'female', 1976, 1995, 5000 ),

  ( 'Ernest Rutherford', 'male', 1871, 1930, 4200 ),

  ( 'Kate Sheppard', 'female', 1847, 1930, 1000 ),

  ( 'Apirana Turupa Ngata', 'male', 1874, 1920, 3500 ),

  ( 'Edmund Hillary', 'male', 1919, 1955, 10000 ),

  ( 'Katherine Mansfield', 'female', 1888, 1920, 2000 ),

  ( 'Margaret Mahy', 'female', 1936, 1985, 5000 ),

  ( 'John Key', 'male', 1961, 1990, 20000 ),

  ( 'Sonny Bill Williams', 'male', 1985, 1995, 15000 ),

  ( 'Dan Carter', 'male', 1982, 1990, 20000 ),

  ( 'Bernice Mene', 'female', 1975, 1990, 30000 );

DROP TABLE IF EXISTS movies;

  CREATE TABLE IF NOT EXISTS movies(
  id INT NOT NULL AUTO_INCREMENT,
  Name_movie VARCHAR(64)NOT NULL,
  year INT NOT NULL,
  length INT NOT NULL,
  director VARCHAR(64),
  name VARCHAR(64),
  price INT,
  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY (name) REFERENCES customs_dbtable2(name)
);



INSERT INTO movies(Name_movie, year,length,director, name, price) VALUE
  ('The Intouchables',2011,112,'Olivier Nakache and Eric Toledano', 'Jane Campion', 2),
  ('From Russia With Love',1963,110,'Terence Young','Margaret Mahy', 4),
  ('The Long Voyage Home',1940,105,'John Ford','Dan Carter', 2),
  ('Easy Rider',1969,94,'Dennis Hopper','Sonny Bill Williams', 2),
  ('Dark Shadows',2012,113,'Tim Burton','Margaret Mahy', 6),
  ('Walk the Line',2005,136,'James Mangold','Kate Sheppard', 2),
  ('The Help',2011,137,'Tate Taylor', 'Bic Runga', 2),
  ('Meet the Parents',2000,107,'Jay Roach', 'Lorde', 5),
  ('The Kings Speech',2011,118,'Tom Hooper','Scribe', 3),
('Charlie and the Chocolate Factory',2005,115,'Tim Burton','Apirana Turupa Ngata', 1),
('Alice In Wonderland',2009,109,'Tim Burton', 'Russell Crowe', 6),
  ('The Iron Lady',2011,105,'Phylliday Lloyd','Jane Campion', 4),
  ('Kaikohe Demolition',2004,52,'Florian Habicht', 'Edmund Hillary', 2),
  ('Brokeback Mountain',2005,134,'Ang Lee', 'Michael Hurst', 3),
  ('Gladiator',2000,154,'Ridley Scott', 'Jane Campion', 3),
  ('The Parent Trap',1961,129,'David Swift', 'Kiri Te Kanawa', 6),
  ('Happy-Go-Lucky',2008,118,'Mike Leigh', 'Anika Moa', 4),
  ('The Big Wedding',2013,89,'Justin Zackham', 'Ernest Rutherford', 7),
  ('Searching for Sugar Man',2012,86,'Malik Bendjelloul', 'John Key', 2),
  ('Going Greek', 2001, 90, 'Justin Zackham', 'Lorde', 4);
#   ('Going Greek', 2001, 90, 'Justin Zackham', 'asdfsdgerger', 4);