-- Answers to Exercise 2 here
DROP TABLE IF EXISTS tb_ex02_JSON;

CREATE TABLE IF NOT EXISTS tb_ex02_JSON (
  username   VARCHAR(64),
  first_name VARCHAR(64),
  last_name  VARCHAR(64),
  email      VARCHAR(64)
);

INSERT INTO tb_ex02_JSON (username, first_name, last_name, email) VALUE
  ('programmer1', 'Bill', 'Gates', 'bill@microsoft.com'),
  ('programmer3', 'Peter', NULL , NULL ),
  ('programmer5', 'Pete', NULL ,NULL ),
  ('programmer2', NULL ,'Peterson', NULL );

SELECT * FROM tb_ex02_JSON;